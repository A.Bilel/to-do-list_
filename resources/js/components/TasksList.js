import axios from 'axios'
    import React, { Component } from 'react'
    import { Link } from 'react-router-dom'

    class ProjectsList extends Component {
      constructor () {
        super()
        this.state = {
          projects: []
        }
        this.handleDelete = this.handleDelete.bind(this);
      }

      componentDidMount () {
        axios.get('/api/tasks').then(response => {
          this.setState({
            projects: response.data
          })
        })
      }

      handleMarkTaskAsCompleted (taskId) {
        axios.put(`/api/tasks/${taskId}`).then(response => {
          this.setState(prevState => ({
            tasks: prevState.tasks.filter(task => {
              return task.id !== taskId
            })
          }))
        })
      }

      handleDelete(id) {
        // remove from local state
        const isNotId = task => task.id !== id;
        const updatedTasks = this.state.tasks.filter(isNotId);
        this.setState({ tasks: updatedTasks });
        // make delete request to the backend
        axios.delete(`/tasks/${id}`);
       }

      render () {
        const { projects } = this.state
        return (
          <div className='container py-4'>
            <div className='row justify-content-center'>
              <div className='col-md-8'>
                <div className='card'>
                  <div className='card-header'>All tasks</div>
                  <div className='card-body'>
                    <Link className='btn btn-primary btn-sm mb-3' to='/create'>
                      Create new task
                    </Link>
                    <ul className='list-group list-group-flush'>
                      {projects.map(project => (
                        <Link
                          className='list-group-item list-group-item-action d-flex justify-content-between align-items-center'
                          to={`/${project.id}`}
                          key={project.id}
                        >
                          {project.title}
                          <button className='btn btn-primary btn-sm' >
                              Mark as completed
                          </button>
                          <button className='btn btn-primary btn-sm' onClick={() => this.handleDelete(project.id)} >
                              Delete
                          </button>
                          <button className='btn btn-primary btn-sm'  onClick={this.handleMarkTaskAsCompleted.bind(this,project.id)} >
                              See more
                          </button>
                        </Link>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
      }
    }

    export default ProjectsList