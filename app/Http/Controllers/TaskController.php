<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;

class TaskController extends Controller
{
    public function index()
    {
      $tasks = Task::all();
      return $tasks->toJson();
    }

    public function store(Request $request)
    {
      $validatedData = $request->validate([
        'name' => 'required',
        'description' => 'required',
      ]);

      $task = Task::create([
        'title' => $validatedData['name'],
        'description' => $validatedData['description'],
      ]);

      return response()->json('Task created!');
    }

    public function markAsCompleted(Task $task)
      {
        $task->is_completed = true;
        $task->update();

        return response()->json('Task updated!');
      }

    public function destroy($id) 
      {
        Task::findOrFail($id)->delete();
      }
}
